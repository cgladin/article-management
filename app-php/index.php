<?php

require __DIR__ . "/vendor/autoload.php";

use App\ArticleRepository;
use App\ArticleService;
use App\UnknownArticleException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;


//echo $test->list();
//$response = new Response($test->list(),Response::HTTP_OK, ["content-type"=>'application/json']);

$request = Request::createFromGlobals();
$path = $request->getPathInfo();
$explode = explode('/',$path);
$service = new ArticleService(new ArticleRepository());
if (in_array($path, ['', '/'])) {
    $list = $service->list();
    for ($i = 0; $i < count($list); $i++) {
        $list2[$i] = ["id"=>$list[$i]->getid(), "title"=>$list[$i]->getTitle(),"text"=> $list[$i]->getText(), "date"=>$list[$i]->getDate(), "author"=>$list[$i]->getAuthor(),"url"=>$list[$i]->getUrl()];
    }
    $response = new JsonResponse($list2, Response::HTTP_OK, ["content-type" => 'application/json']);
} elseif ($explode[1] === "article" && isset($explode[2]) && !isset($explode[3])) {
    try{
        $list=$service->get($explode[2]-1);
        $temp = ["id"=>$list->getid(), "title"=>$list->getTitle(),"text"=> $list->getText(), "date"=>$list->getDate(), "author"=>$list->getAuthor(),"url"=>$list->getUrl()];
        $response = new JsonResponse($temp, Response::HTTP_OK, ["content-type" => 'application/json']);
    } catch (UnknownArticleException $e){
        $response = new JsonResponse("Article not found",500, ["content-type" => 'application/json']);
    }

} else {
    $response = new Response('Page not found.', Response::HTTP_NOT_FOUND);
}
$response->send();