<?php


namespace App;

class ArticleService
{
    public ArticleRepository $articleRepository;

    /**
     * ArticleService constructor.
     * @param ArticleRepository $articleRepository
     */
    public function __construct(ArticleRepository $articleRepository)
    {
        $this->articleRepository = $articleRepository;
    }

    public function list()
    {
        return $this->articleRepository->findAll();
    }

    public function get(int $index)
    {
        return $this->articleRepository->get($index);
    }
}